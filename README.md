# Important updates for HW5

* You are required to write a function which will clean up your linked list(s)
  before the program terminates. Your program must correctly and completely
  clean up after itself, and Valgrind must report no leaks in your own code.
  Leaks in standard libararies are not your responsibility.

* Your program must not abort on memory errors as found by the Address
  Sanitizer, (enabled by g++'s -fsanitize=address flag).  We'll look for this
  as we grade your submission; this means that you should test this yourself.


# Sample program output for HW5

Look at the files under the sample_output/ directory to see what your program's
final output ought to look like, as well as what Valgrind might report for your
program when a memory leak is present.

The output of your program when built with -fsanitize=address should be
identical to its ordinary output. In other words, ASAN should not find any
problems at all.

