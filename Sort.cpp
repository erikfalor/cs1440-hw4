

#include "MergeSort.hpp"


/* You must implement comparators to use with listsort() which implement stable
 * sorts which fall-back to the FIPS area code as a secondary key to break ties.
 *
 * Hint: The signature of comparators used with listsort() are identical to the
 * comparators used with the standard qsort() function
 */

/*
 * Here is another example of a comparator. This comparator can order two
 * Employment objects based upon their FIPS area code. This might just come in
 * handy later.
 */
static int cmp_employment_area_fips(const void *left, const void *right) {
	if ((static_cast<const Employment*>(left))->area_fips < (static_cast<const Employment*>(right))->area_fips)
		return -1;
	else if ((static_cast<const Employment*>(left))->area_fips > (static_cast<const Employment*>(right))->area_fips)
		return 1;
	else
		return 0;
}



static int cmp_employment_emplvl(const void *left, const void *right) {
	if ((static_cast<const Employment*>(left))->annual_avg_emplvl < (static_cast<const Employment*>(right))->annual_avg_emplvl)
		return -1;
	else if ((static_cast<const Employment*>(left))->annual_avg_emplvl > (static_cast<const Employment*>(right))->annual_avg_emplvl)
		return 1;
	else
		return cmp_employment_area_fips(left, right);
}

Employment* sort_empl_by_annual_avg_emplvl(Employment *list) {
    return mergesort(list, cmp_employment_emplvl);
}



static int cmp_employment_wages(const void *left, const void *right) {
	if ((static_cast<const Employment*>(left))->total_annual_wages < (static_cast<const Employment*>(right))->total_annual_wages)
		return -1;
	else if ((static_cast<const Employment*>(left))->total_annual_wages > (static_cast<const Employment*>(right))->total_annual_wages)
		return 1;
	else
		return cmp_employment_area_fips(left, right);
}

Employment* sort_empl_by_total_annual_wages(Employment *list) {
    return mergesort(list, cmp_employment_wages);
}
