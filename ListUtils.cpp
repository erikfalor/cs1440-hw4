#include <iostream>
#include <fstream>

#include "ListUtils.hpp"


Employment* build_empl_list(std::string filename) {
	std::ifstream employmentf(filename);

	// throw away the 1st line
	{
		std::string junk;
		std::getline(employmentf, junk);
	}

	std::string	  area_fips;
	unsigned      annual_avg_emplvl;
	long unsigned total_annual_wages;

	// prime the pump
	employmentf >> area_fips
		>> annual_avg_emplvl
		>> total_annual_wages;

	Employment* head = new Employment(area_fips,
			annual_avg_emplvl,
			total_annual_wages);
	Employment* follow = head;

	while ( employmentf >> area_fips
			>> annual_avg_emplvl
			>> total_annual_wages) {

		follow->next = new Employment(area_fips,
			annual_avg_emplvl,
			total_annual_wages);

		follow = follow->next;
	}

	return head;
}


void append_lists(Employment* head, Employment* tail) {
    while (head->next)
        head = head->next;
    head->next = tail;
}


int list_length(Employment *emp) {
    int i = 0;
    while (emp != (Employment*) NULL) {
        ++i;
        emp = emp->next;
    }
    return i;
}


void print_every_empl(Employment *emp) {
    while (emp != (Employment*) NULL) {
        std::cout << *emp;
        emp = emp->next;
    }
}
