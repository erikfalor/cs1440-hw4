This output illustrates what valgrind may report when your code contains no
memory leaks. Notice that it still reports some leaked bytes under the "still
reachable" category; because of the '--leak-check=full' flag we can determine
that these bytes were not leaked by our own code but by the underlying runtime
library. Therefore, this problem isn't our concern.

    $ valgrind --leak-check=full --show-reachable=yes ./main

==938== Memcheck, a memory error detector
==938== Copyright (C) 2002-2013, and GNU GPL'd, by Julian Seward et al.
==938== Using Valgrind-3.10.1 and LibVEX; rerun with -h for copyright info
==938== Command: ./main
==938== 
[============]
[Final Report]
[============]

Statistics over all industries in 2016:
=======================================
Number of FIPS areas in report     3271

Total annual wages                 $7633223106028
Minimum annual wage                $2370739
Median annual wage                 $298745960
Maximum annual wage                $274236493774

Total annual employment level      142801173
Median annual employment level     8089
Maximum annual employment level    4344132
Minimum annual employment level    74


==838== 
==838== HEAP SUMMARY:
==838==     in use at exit: 72,704 bytes in 1 blocks
==838==   total heap usage: 10,038 allocs, 10,037 frees, 861,102 bytes allocated
==838== 
==838== 72,704 bytes in 1 blocks are still reachable in loss record 1 of 1
==838==    at 0x4C2AB80: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==838==    by 0x4E9DBD5: ??? (in /usr/lib/x86_64-linux-gnu/libstdc++.so.6.0.24)
==838==    by 0x40102D9: call_init.part.0 (dl-init.c:78)
==838==    by 0x40103C2: call_init (dl-init.c:36)
==838==    by 0x40103C2: _dl_init (dl-init.c:126)
==838==    by 0x4001299: ??? (in /lib/x86_64-linux-gnu/ld-2.19.so)
==838== 
==838== LEAK SUMMARY:
==838==    definitely lost: 0 bytes in 0 blocks
==838==    indirectly lost: 0 bytes in 0 blocks
==838==      possibly lost: 0 bytes in 0 blocks
==838==    still reachable: 72,704 bytes in 1 blocks
==838==         suppressed: 0 bytes in 0 blocks
==838== 
==838== For counts of detected and suppressed errors, rerun with: -v
==838== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
