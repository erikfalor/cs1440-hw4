This output was created by running the command

	$ valgrind --show-reachable=yes ./main



==1240== Memcheck, a memory error detector
==1240== Copyright (C) 2002-2013, and GNU GPL'd, by Julian Seward et al.
==1240== Using Valgrind-3.10.1 and LibVEX; rerun with -h for copyright info
==1240== Command: ./main
==1240== 
[============]
[Final Report]
[============]

Statistics over all industries in 2016:
=======================================
Number of FIPS areas in report     3271

Total annual wages                 $7633223106028
Minimum annual wage                $2370739
Median annual wage                 $298745960
Maximum annual wage                $274236493774

Total annual employment level      142801173
Median annual employment level     8089
Maximum annual employment level    4344132
Minimum annual employment level    74


==1240== 
==1240== HEAP SUMMARY:
==1240==     in use at exit: 275,506 bytes in 6,543 blocks
==1240==   total heap usage: 10,038 allocs, 3,495 frees, 861,102 bytes allocated
==1240== 
==1240== LEAK SUMMARY:
==1240==    definitely lost: 32 bytes in 1 blocks
==1240==    indirectly lost: 202,770 bytes in 6,541 blocks
==1240==      possibly lost: 0 bytes in 0 blocks
==1240==    still reachable: 72,704 bytes in 1 blocks
==1240==         suppressed: 0 bytes in 0 blocks
==1240== Rerun with --leak-check=full to see details of leaked memory
==1240== 
==1240== For counts of detected and suppressed errors, rerun with: -v
==1240== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
