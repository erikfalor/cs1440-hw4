#include <iostream>

#include "Report.hpp"
#include "DirToList.hpp"
#include "Employment.hpp"
#include "ListUtils.hpp"
#include "Stats.hpp"
#include "Sort.hpp"

// include other header files as needed


int main(void) {
    Report rpt;


    Employment *head = DirToList("database");

    rpt.num_areas    = list_length(head);

    rpt.total_wages  = total_annual_wages(head);
    rpt.min_wages    = min_annual_wages(head);
    rpt.max_wages    = max_annual_wages(head);

    head = sort_empl_by_total_annual_wages(head);
    rpt.med_wages    = med_annual_wages(head);

    rpt.total_emplvl = total_annual_emplvl(head);
    rpt.min_emplvl   = min_annual_emplvl(head);
    rpt.max_emplvl   = max_annual_emplvl(head);

    head = sort_empl_by_annual_avg_emplvl(head);
    rpt.med_emplvl   = med_annual_emplvl(head);


    std::cout << rpt << std::endl;
}
