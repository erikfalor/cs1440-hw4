#include <limits.h> // provides UINT_MAX, ULONG_MAX

#include "Stats.hpp"
#include "ListUtils.hpp"



unsigned long total_annual_wages(Employment *emp) {
	unsigned long wages = 0;

	for (; emp; emp = emp->next)
		wages += emp->total_annual_wages;

	return wages;
}


unsigned long min_annual_wages(Employment *emp) {
	unsigned long min = ULONG_MAX;
	for (; emp; emp = emp->next)
		if (emp->total_annual_wages < min)
			min = emp->total_annual_wages;
	return min;
}


// Assume that the list has been sorted appropriately
unsigned long med_annual_wages(Employment *emp) {
    for (int middle = list_length(emp) / 2, i = 0; i < middle; ++i)
        emp = emp->next;

    return emp->total_annual_wages;
}


unsigned long max_annual_wages(Employment *emp) {
	unsigned long max = 0UL;

	for (; emp; emp = emp->next)
		if (emp->total_annual_wages > max)
			max = emp->total_annual_wages;

	return max;
}


unsigned total_annual_emplvl(Employment *emp) {
	unsigned emplvl = 0;

	for (; emp; emp = emp->next)
		emplvl += emp->annual_avg_emplvl;

	return emplvl;
}


unsigned min_annual_emplvl(Employment *emp) {
	unsigned min = UINT_MAX;

	for (; emp; emp = emp->next)
		if (emp->annual_avg_emplvl < min)
			min = emp->annual_avg_emplvl;

	return min;
}


// Assume that the list has been sorted appropriately
unsigned med_annual_emplvl(Employment *emp) {
    int middle = list_length(emp) / 2;
    for (int i = 0; i < middle; ++i)
        emp = emp->next;

    return emp->annual_avg_emplvl;
}


unsigned max_annual_emplvl(Employment *emp) {
	unsigned max = 0U;

	for (; emp; emp = emp->next)
		if (emp->annual_avg_emplvl > max)
			max = emp->annual_avg_emplvl;

	return max;
}
